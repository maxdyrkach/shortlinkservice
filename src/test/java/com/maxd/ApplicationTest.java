package com.maxd;

import com.maxd.controllers.LinkControllerTest;
import com.maxd.controllers.RedirectControllerTest;
import com.maxd.controllers.StatsControllerTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


//главный тестовый класс, запускает остальные тесты из /controllers
@RunWith(Suite.class)
@Suite.SuiteClasses({LinkControllerTest.class, RedirectControllerTest.class, StatsControllerTest.class})

public class ApplicationTest {

}
