package com.maxd.controllers;


import com.maxd.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//тестирует RedirectController
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class RedirectControllerTest {

    @Autowired
    private MockMvc mockMvc;
    public List<String> originalTestLinks = new LinkedList<>(Arrays.asList("www.google.com", "www.yahoo.com", "www.facebook.com",
            "www.twitter.com", "www.vk.com"));

    //генерирует запросы на переадресацию в определенном количестве и проверяет ответы
    @Test
    public void redirectControllerTest() throws Exception {
        int counter = 1;
        int iterations = 10;
        for (String originalLink : originalTestLinks) {
            for (int i = 0; i < iterations; i++) {
                this.mockMvc.perform(get("/l/" + counter)).andDo(print()).andExpect(status().is3xxRedirection())
                        .andExpect(redirectedUrl("http://" + originalLink));
            }
            counter++;
            iterations--;

        }
    }
}
