package com.maxd.controllers;


import com.maxd.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


//тестирует StatsController
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class StatsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private List<String> originalTestLinks = new LinkedList<>(Arrays.asList("www.google.com", "www.yahoo.com", "www.facebook.com",
            "www.twitter.com", "www.vk.com"));

    //запрашивает статистику по каждому адресу и проверяет все параметры ответа
    @Test
    public void statsTest() throws Exception {
        int counter = 1;
        int iterations = 10;
        for (String originalLink : originalTestLinks) {
            this.mockMvc.perform(get("/stats/" + counter)).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.link", is("/l/" + counter)))
                    .andExpect(jsonPath("$.original", is(originalLink)))
                    .andExpect(jsonPath("$.rank", is(counter)))
                    .andExpect(jsonPath("$.count", is(iterations)));
            counter++;
            iterations--;

        }
    }

    //запрашивает рейтинги в виде страниц
    @Test
    public void ratingTest() throws Exception {
        for (int i = 1, iter = 10; i <= 5; i++, iter--) {
            this.mockMvc.perform(get("/stats/?page=" + i + "&count=1")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$", hasSize(1)))
                    .andExpect(jsonPath("$[0].link", is("/l/" + i)))
                    .andExpect(jsonPath("$[0].original", is(originalTestLinks.get(i - 1))))
                    .andExpect(jsonPath("$[0].rank", is(i)))
                    .andExpect(jsonPath("$[0].count", is(iter)));
        }


        this.mockMvc.perform(get("/stats/?page=2&count=2")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].link", is("/l/3")))
                .andExpect(jsonPath("$[0].original", is(originalTestLinks.get(2))))
                .andExpect(jsonPath("$[0].rank", is(3)))
                .andExpect(jsonPath("$[0].count", is(8)))
                .andExpect(jsonPath("$[1].link", is("/l/4")))
                .andExpect(jsonPath("$[1].original", is(originalTestLinks.get(3))))
                .andExpect(jsonPath("$[1].rank", is(4)))
                .andExpect(jsonPath("$[1].count", is(7)));
    }


}
