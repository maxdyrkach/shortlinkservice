package com.maxd.controllers;

import com.maxd.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
@AutoConfigureMockMvc
public class LinkControllerTest {

    @Autowired
    private MockMvc mockMvc;

    //список тестовых оригинальных адресов
    private List<String> originalTestLinks = new LinkedList<>(Arrays.asList("www.google.com","www.yahoo.com","www.facebook.com",
            "www.twitter.com","www.vk.com"));

   //запрашивает короткие ссылки и проверяет ответы
    @Test
    public void linkControllerTest() throws Exception{

        int counter=1;
        for (String originalLink: originalTestLinks){
            doOneLinkTest(originalLink,String.valueOf(counter));
            counter++;
        }

        doOneLinkTest("www.facebook.com","3");
        doOneLinkTest("www.google.com","1");
        this.mockMvc.perform(post("/generate").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"original\":\"\"}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());


    }


    //вспомогательный метод
    private void doOneLinkTest(String originalUrl, String expectedNumber) throws Exception {
        this.mockMvc.perform(post("/generate").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"original\":\""+originalUrl+"\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.link", is("/l/" + expectedNumber)));

    }


}