package com.maxd.controllers;


import com.maxd.Services.DatabaseService;
import com.maxd.models.LinkEntity;
import com.maxd.repos.LinkRepository;
import com.sun.deploy.net.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.lang.model.type.ErrorType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class RedirectController {

    //@Autowired
    //private LinkRepository repository;

    @Autowired
    private DatabaseService dbService;
    //перенаправляет с короткой ссылки на оригинальную по GET запросу /l/короткая_ссылка
    @GetMapping(value = "/l/*")
    public RedirectView redirect(HttpServletRequest request) {

        //в переменную link помещаем номер короткой ссылки из БД
        String url = request.getRequestURI().substring(3);
        Long link = Long.parseLong(url);

        //извлекаем сущность из БД по короткому номеру (link)
        LinkEntity entity = dbService.findByLinkIs(link);

        //если она существует, увеличиваем счетчик count количества обращений по ссылке и сохраняем в БД
        if (entity != null) {
            entity.incrementCount();
            entity = dbService.save(entity);
            String originalLink = entity.getOriginal();

            //для перенаправления проверяем полный ли адрес был передан изначально, если нет - дополняем
            if (originalLink.startsWith("http://") || originalLink.startsWith("https://")) {
                return new RedirectView(originalLink);
            } else
                return new RedirectView("http://" + originalLink);
        }

        //если в БД нет такой короткой ссылки, ничего не возвращаем
        return new RedirectView();
    }
}
