package com.maxd.controllers;


import com.maxd.Services.DatabaseService;
import com.maxd.models.LinkEntity;
import com.maxd.models.LinkResponse;
import com.maxd.models.OriginalLink;
import com.maxd.repos.LinkRepository;
import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class LinkController {

    //@Autowired
    //private LinkRepository repository;

    @Autowired
    private DatabaseService dbService;
    //возвращает json c короткой ссылкой по запросу /generate?original=длинная_ссылка
    @RequestMapping(value = "/generate", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public LinkResponse response(@RequestBody OriginalLink originalLink, HttpServletResponse response) {

        //if ((originalLink.getOriginal()).trim().equals("")){
        URLValidator validator = new URLValidator();
        if (validator.isValid(originalLink.getOriginal(),null)){
            try {
                response.sendError(400, "Request json parameter is empty!");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        //создаем экземпляр сущности и получаем в нее даные из БД по оригинальной ссылке

        LinkEntity entity = dbService.findByOriginalIs(originalLink.getOriginal());

        /* если такая сущность отсутствует в БД, создаем новую и сохраняем ее в БД,
         *одновременно обновляя саму сущность возвращаемым значение (с присвоенным link)
         * возвращаем json из сущности LinkResponse
         */
        if (entity == null) {
            entity = new LinkEntity(originalLink.getOriginal());
            entity = dbService.save(entity);
        }
            return new LinkResponse("/l/" + entity.getLink());

    }
}

