package com.maxd.controllers;

import com.maxd.Services.DatabaseService;
import com.maxd.models.LinkEntity;
import com.maxd.Services.StatsHelper;
import com.maxd.models.StatsResponse;
import com.maxd.repos.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RestController

public class StatsController {

    //@Autowired
    //private LinkRepository repository;

    @Autowired
    private DatabaseService dbService;


    /*возвращает json объект со статистикой по одной ссылке на запрос: /stats/l/короткая_ссылка
    */
    @RequestMapping(value = "/stats/*", method = RequestMethod.GET)
    public StatsResponse stats(HttpServletRequest request) {

        //в переменную link помещаем номер короткой ссылки из запроса и извлекаем сущность из БД
        String requestLink = request.getRequestURI().substring(7);
        Long link = Long.parseLong(requestLink);
        LinkEntity entity = dbService.findByLinkIs(link);

        //если она существует, получаем rank и возвращаем объект StatsResponse  в виде json
        if (entity!=null){
            List<LinkEntity> list = new ArrayList<>();
            list.addAll(dbService.findAll());
            StatsHelper statsHelper=new StatsHelper();
            int rank=statsHelper.getRank(list,link);
            return new StatsResponse("/l/"+entity.getLink().toString(), entity.getOriginal(), rank, entity.getCount());
        }
        return null;

    }

    /*возвращает массив json со статистикой по запросу: /stats?page=страница&count=количество_записей
    *записей не более 100*/
    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public List<StatsResponse> rating(HttpServletRequest request, HttpServletResponse response, @RequestParam(value="page", required = true) int page,
                                   @RequestParam(value="count", required = true) int count) throws IOException {

        //проверяем запрошенное кол-во записей
        if(count>100||count<1) {
            response.sendError(400, "Count is wrong!");
            return null;
        }

        //проверяем номер страницы
        if (page<1) {
            response.sendError(400, "Number of page is wrong! Must be grater than 0");
            return null;
        }
        List<LinkEntity> list = dbService.findAll();
        int pages = (int) Math.ceil((float) list.size() / (float) count);
        if(page>pages) {
            response.sendError(400, "Number of page is wrong! Maximum number is "+pages);
            return null;
        }

        /*если все проверки успешны, с помощью метода вспомогательного класса
        *получаем лист объектов json, его же возвращаем*/
        StatsHelper helper=new StatsHelper();
        List<StatsResponse> statsList=new LinkedList<>();
        try {
                list = helper.getPageList(list, page, count);
                statsList = helper.toStatsList(list);
            } catch (Exception e) {e.printStackTrace();}

        return statsList;
    }
}

