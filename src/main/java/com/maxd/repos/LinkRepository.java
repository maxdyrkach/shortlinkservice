package com.maxd.repos;


import com.maxd.models.LinkEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//репозиторий БД
@Repository
public interface LinkRepository extends JpaRepository<LinkEntity, Long> {

    //ищет в БД по короткой ссылке
    LinkEntity findByLinkIs(Long link);

    //ищет в БД по оригинальной ссылке
    LinkEntity findByOriginalIs(String original);
}
