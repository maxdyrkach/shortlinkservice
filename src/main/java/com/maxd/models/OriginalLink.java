package com.maxd.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
//@JsonIgnoreProperties (ignoreUnknown = true)
public class OriginalLink {

    private String original;

    public OriginalLink(){}

    public OriginalLink(String original){
        this.original=original;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
