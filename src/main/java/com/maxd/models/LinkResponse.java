package com.maxd.models;

import org.springframework.stereotype.Component;

//сущность для возврата короткой ссылки в виде json

@Component
public class LinkResponse {

    private String link;

    public LinkResponse(){}

    public LinkResponse(String link){
        this.link=link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String shortLink) {
        this.link = link;
    }
}
