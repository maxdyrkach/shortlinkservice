package com.maxd.models;


import org.springframework.stereotype.Component;


//сущность для возврата json объектов в статистике
@Component
public class StatsResponse {
    private String link;
    private String original;
    private int rank;
    private long count;

    public StatsResponse() {
    }

    public StatsResponse (String link, String original, int rank, long count){
        this.link=link;
        this.original=original;
        this.rank=rank;
        this.count=count;
    }

    public String getLink() {
        return link;
    }

    public String getOriginal() {
        return original;
    }

    public int getRank() {
        return rank;
    }

    public long getCount() {
        return count;
    }


}
