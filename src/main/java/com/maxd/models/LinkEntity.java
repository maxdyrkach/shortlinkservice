package com.maxd.models;

import javax.persistence.*;


//сущность, хранимая в БД
@Entity
public class LinkEntity {

    //поле, автоматически генерируетя при создании записи в БД, является одновременно основой для короткой ссылки
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long link;

    //оригинальная ссылка
    @Column(nullable = false)
    private String original;

    //количество переходов по ссылке
    private Integer count;

    //место в рейтинге (рассчитывается по факту обращения, в БД хранятся нули)
    private Integer rank;

    public LinkEntity() {
    }

    public LinkEntity(LinkEntity that) {
        this.original = new String(that.original);
        this.link = new Long(that.link);
        this.count = new Integer(that.count);
        this.rank = new Integer(that.rank);
    }

    public LinkEntity(String originalLink) {
        this.original = originalLink;
        this.link = 0l;
        this.count = 0;
        this.rank = 0;
    }

    public LinkEntity(Long link, String originalLink, Integer count, Integer rank) {
        this.original = originalLink;
        this.link = link;
        this.count = count;
        this.rank = rank;
    }


    //метод для инкремента счетчика переходов по ссылке
    public Integer incrementCount() {
        this.count = new Integer(this.count.intValue() + 1);
        return this.count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getOriginal() {

        return original;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getRank() {
        return rank;
    }

    public Long getLink() {
        return link;
    }

    public void setLink(Long link) {
        this.link = link;
    }
}
