package com.maxd.Services;

import com.maxd.models.LinkEntity;
import com.maxd.repos.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class DatabaseService {

    @Autowired
    private LinkRepository repository;

    public DatabaseService (){ }

    @Transactional
    public synchronized LinkEntity save (LinkEntity entity){
        synchronized (repository) {
            return repository.save(entity);
        }
    }


    public LinkEntity findByLinkIs(Long link){
        synchronized (repository) {
            return repository.findByLinkIs(link);
        }
    }

    public  LinkEntity findByOriginalIs(String originalLink){
        synchronized (repository){
        return repository.findByOriginalIs(originalLink);
        }
    }


    public  List<LinkEntity> findAll() {
        List<LinkEntity> list = new ArrayList<>();
        synchronized (repository) {
            list.addAll(repository.findAll());
        }
        return list;
    }
}
