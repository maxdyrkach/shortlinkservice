package com.maxd.Services;

import com.maxd.models.LinkEntity;
import com.maxd.models.StatsResponse;
import com.maxd.repos.LinkRepository;
import org.springframework.beans.factory.parsing.BeanEntry;


import java.util.*;

//вспомогательный класс для StatsController
public class StatsHelper {

  //расчитывает место в рейтинге rank
       public int getRank(List<LinkEntity> list, Long link) {


        list=sortListDesc(list);

        long pcount = 0;
        long ccount = 0;
        int prank = 0;
        int crank = 0;
        for (int i = 0; i < list.size(); i++) {
            LinkEntity entity = list.get(i);

            if (i == 0) {
                pcount = entity.getCount();
                crank = 1;
                entity.setRank(crank);
                prank = crank;
            } else {
                ccount = entity.getCount();
                if (ccount < pcount) {
                    crank++;
                    entity.setRank(crank);
                    prank = crank;
                    pcount = ccount;
                } else if (ccount == pcount) {
                    entity.setRank(crank);
                }
            }
            if (entity.getLink().equals(link)){
                return crank;
            }
        }
        return 0;
    }

    //возвращает страницу с рейтингом ссылок для представления в виде массива json
    public List<LinkEntity> getPageList (List<LinkEntity> list, int page, int count) throws Exception {

        if (list==null || page<1 || count<1){
            throw new IllegalArgumentException("Неверное значение агрумента");
        }

        List<LinkEntity> pageList = new ArrayList<>();
        list=sortListDesc(list);

        long pcount = 0;
        long ccount = 0;
        int prank = 0;
        int crank = 0;
        for (int i = 0; i < list.size(); i++) {
            LinkEntity entity = list.get(i);

            if (i == 0) {
                pcount = entity.getCount();
                crank = 1;
                entity.setRank(crank);
                prank = crank;
            } else {
                ccount = entity.getCount();
                if (ccount < pcount) {
                    crank++;
                    entity.setRank(crank);
                    prank = crank;
                    pcount = ccount;
                } else if (ccount == pcount) {
                    entity.setRank(crank);
                }
            }
        }
        int pages = (int) Math.ceil((float) list.size() / (float) count);

        if (pages == 1) {

            return list;
        } else if (page < pages) {
            int beginVal = ((page - 1) * count);
            int endVal = beginVal + count;
            pageList = list.subList(beginVal, endVal);
            return pageList;
        } else if (page == pages) {
            int beginVal = ((page - 1) * count);
            list.add(list.size(), new LinkEntity());
            int endVal = list.size() - 1;
            pageList = list.subList(beginVal, endVal);
            return pageList;
        } else
            throw new Exception();

    }

    // возвращает лист сущностей LinkEntity в виде объектов StatsResponse
    public List<StatsResponse> toStatsList(List<LinkEntity> list){
        List<StatsResponse> statsList = new LinkedList<StatsResponse>();
        LinkEntity temp= new LinkEntity();

        for (int i=0; i<list.size(); i++){
            temp=list.get(i);
            statsList.add(new StatsResponse("/l/"+temp.getLink().toString(),temp.getOriginal(),temp.getRank(),temp.getCount()));
        }
        return statsList;
    }


    //сортирует лист в обратной порядке
    public List<LinkEntity> sortListDesc (List<LinkEntity> list){
        Collections.sort(list, (Comparator<LinkEntity>) (e1, e2) -> {
                    int res = (int) (e2.getCount()-e1.getCount());
                    return res;
                }
        );
        return list;
    }




}
